'use strict';


// すべてのタブを取得
// chrome.tabs.query({}, function(tabs){
// ウインドウ毎のラストフォーカスタブを指定
// chrome.tabs.query({lastFocusedWindow:true}, function(tabs){
// 現在のタブを指定
chrome.tabs.query({active:true ,lastFocusedWindow:true}, function(tabs){

	var i;
	var results = document.getElementById('results');
	var titles = [];
	for(i = 0; i < tabs.length; i++){
		//console.log(tabs[i].title);
		titles.push(tabs[i].title);
	}
	results.value = titles.join("\n");
	results.select();
});